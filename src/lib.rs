//! Building blocks for a full featured simulation GUI
///
/// The provided structures can be inserted into a larger GUI project and used
/// as wrappers for repetitive tasks.

mod plot;
pub use plot::GTKPlot;
mod inspector;
pub use inspector::Inspector;


/// Makes it easier to get objects from GtkBuilder
///
/// Returns with the requested object or panics if cannot find the object by its
/// name. Panic message contains the requested object name.
#[macro_export]
macro_rules! get_object {
    ( $builder:tt, $name:tt ) => {
        $builder.get_object($name).expect(&format!("Cannot find '{}' in ui file)", $name))
    }
}

#[cfg(test)]
mod tests {
}
