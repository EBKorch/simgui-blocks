use gtk::prelude::*;
use sim_framework::{
    measurement::{ Frame, Property },
    simtrait::SimItem,
    simulation::Simulation,
};
use super::{
    GTKPlot,
    get_object
};

type UpdateMap = Box<dyn Fn(&Simulation, &Box<dyn SimItem>) -> String>;
type UpdatePlot = Box<dyn Fn(&Simulation, &Box<dyn SimItem>) -> Vec<Frame<Property>>>;

/// Helper to create updatable informative sections and plots
///
/// Sections and plots are pre-styled, and generally the structure provides
/// little configuration options as it is intended to be us high-level
/// component.
///
/// Building a section is started by opening it, giving a title and description.
/// After that you can add new rows to it which are key-value pairs, or plots.
/// After finished with a section you are expected to close it before starting
/// new sections.
///
/// To insert it into your GUI you can use the rows attribute which contains the
/// top level elements of the built element. It is best to insert them into a 
/// GTKList or similar parent widget.
///
/// You have to types of data inside the `Inspector`: static and live. The
/// difference between the two is that static rows will never change, while
/// live data can be changed by the update method.
///
/// For live data you have to pass the simulation and the simulated item. This
/// is a requirement from the Inspector implementation: when a live row or plot
/// is added the Inspector builds a mapping which will later be used by the
/// update method to identify the required labels and drawing areas.
///
/// # Examples
///
/// ## Create a single static row
/// ```
/// # use simgui_blocks::*;
/// // We need to initialize GTK otherwise no widgets can be created. When you
/// // are building your own GUI you don't have to do this as GTK will be
/// // initialized when the application is created.
/// use gtk::prelude::*;
/// gtk::init();
/// let mut inspector = Inspector::new();
/// let title = "Example row";
/// let description = "This is created for the documentation";
/// inspector.new_section(title, description);
/// inspector.add_static_row("Key", "Value");
/// inspector.close_section();
/// assert_eq!(inspector.rows.len(), 1);
/// ```
pub struct Inspector {
    section: Option<Section>,
    pub rows: Vec<gtk::Widget>,
    update_map: Vec<(gtk::Label, UpdateMap)>,
    plot_map: Vec<(GTKPlot, UpdatePlot)>,
    update_frequency: usize,
    update_counter: usize,
}

impl Inspector {
    /// Creates a new `Inspector`
    pub fn new() -> Self {
        Inspector {
            section: None,
            rows: vec![],
            update_map: vec![],
            plot_map: vec![],
            update_frequency: 1,
            update_counter: 0,
        }
    }

    // Creates a new builder from the template ui file
    fn builder() -> gtk::Builder {
        gtk::Builder::from_string(include_str!("../resources/section.ui"))
    }

    /// Starts a new section
    ///
    /// If there is an open section it will be closed and added to the rows.
    ///
    /// # Arguments
    ///
    /// * `title` - Title of the section, will appear as large bold text at the
    /// beginning
    /// * `description` - Description of the section, will appear as smaller and
    /// lighter text below the title
    pub fn new_section(&mut self, title: &str, description: &str) {
        // Close the previous section if any
        if self.section.is_some() { self.close_section(); }
        
        // Open a new section
        self.section = Some(Section::new(title, description));
    }

    /// Adds a new static row to the section
    ///
    /// This will panic if there is no open section.
    ///
    /// Both the name and the value must be a string, so formatting should take
    /// place before passing values to the function.
    ///
    /// # Arguments
    ///
    /// `name`: Descriptive name of the displayed value. Will appear at the left
    /// side of the row
    /// `value`: Vale of the variable. Will appear at the right side of the row
    pub fn add_static_row(&mut self, name: &str, value: &str) {
        if let Some(section) = &self.section {
            let _ = section.add_row(name, value);
        } else { panic!("There is no section where lines can be inserted" )}
    }

    /// Adds an updatable row to the section
    ///
    /// Panics of there is no open section
    ///
    /// You must pass both the simulation and and the item to the function, as
    /// well as define a closure which retrieves and formats the value to be
    /// displayed.
    ///
    /// # Arguments
    ///
    /// `name` - Display name of the variable. This is static and not updatable.
    /// `sim` - The simulation instance
    /// `item` - Selected `SimItem`
    /// `fnc` - Update function which retrieves the data using the simulation
    /// and the simulated item.
    pub fn add_live_row(&mut self, name: &str, sim: &Simulation, item: &Box<dyn SimItem>, fnc: UpdateMap) {
        if let Some(section) = &self.section {
            let valuelabel = section.add_row(name, &*fnc(sim, item));
            self.update_map.push((valuelabel, fnc));
        } else { panic!("There is no section where lines can be inserted" )}
    }

    /// Closes an open section and adds it to the `rows`
    ///
    /// Panics if there is no open section.
    pub fn close_section(&mut self) {
        if self.section.is_none() { panic!("There is no pending section") }

        let content = std::mem::replace(&mut self.section, None).unwrap();

        self.rows.push(gtk::Frame::upcast(content.finalize()));
    }

    /// Updates every data in the `Inspector`
    ///
    /// This function uses the mapped `fnc` arguments (passed as input argument
    /// at `add_live_plot` and `add_live_item` functions) to update GUI
    /// elements.
    ///
    /// # Arguments
    ///
    /// `sim` - The simulation instance
    /// `item` - Currently inspected item
    pub fn update(&mut self, sim: &Simulation, item: &Box<dyn SimItem>) {
        self.update_counter += 1;
        if self.update_counter > self.update_frequency {
            self.update_counter = 0;
            for (label, fnc) in &self.update_map {
                label.set_label(&*fnc(sim, item));
            }
            for (plot, fnc) in &mut self.plot_map {
                plot.update_data(fnc(sim, item));
            }
        }
    }

    /// Adds a plot with static data
    ///
    /// Panics if there is no open section
    pub fn add_plot(&mut self, frames: Vec<Frame<Property>>) {
        if let Some(section) = &self.section {
            section.add_plot(frames);
        } else { panic!("There is no section where the plot could be inserted")}
    }

    /// Adds an updatable plot to the section
    ///
    /// Panics if there is no open section.
    ///
    /// # Arguments
    ///
    /// `sim` - Simulation instance
    /// `item` - Currently inspected item
    /// `fnc` - Function which retrieves frames using the simulation and the
    /// item. This is also stored to later be used to update the plot.
    pub fn add_live_plot(&mut self, sim: &Simulation, item: &Box<dyn SimItem>, fnc: UpdatePlot) {
        if let Some(section) = &self.section {
            let plot = section.add_plot(fnc(sim, item)).unwrap();
            self.plot_map.push((plot, fnc));
        } else { panic!("There is no section where the plot could be inserted")}
    }
}

struct Section {
    frame: gtk::Frame,
    content: gtk::ListBox,
}

impl Section {
    pub fn new(title: &str, comment: &str) -> Self {
        // Load the builder for the gtk widgets
        let builder = Inspector::builder();

        // Outter frame
        let frame: gtk::Frame = get_object!(builder, "frame");

        // Set title
        let title_label: gtk::Label = get_object!(builder, "title");
        title_label.set_label(title);

        // Set description
        let comment_label: gtk::Label = get_object!(builder, "comment");
        comment_label.set_label(comment);

        // Remove lines unneeded lines
        let content: gtk::ListBox = get_object!(builder, "content_list");
        content.remove(&content.get_children()[0]);

        Section { frame, content }
    }

    pub fn add_row(&self, name: &str, value: &str) -> gtk::Label {
        let builder = Inspector::builder();

        // Get the row and add
        let row: gtk::ListBoxRow = get_object!(builder, "example_row");
    
        // Remove it from its container
        let container: gtk::ListBox = get_object!(builder, "content_list");
        container.remove(&row);

        // Set the name of the property
        let titlelabel: gtk::Label = get_object!(builder, "example_name");
        titlelabel.set_label(name);

        // Set the value
        let valuelabel: gtk::Label = get_object!(builder, "example_value");
        valuelabel.set_label(value);

        // Add it to the row
        self.content.add(&row);

        // Return with the value label
        valuelabel
    }

    pub fn add_plot(&self, frames: Vec<Frame<Property>>) -> Option<GTKPlot> {
        if frames.len() > 0 { match &frames[0].value {
            Property::Float(_) => (),
            _ => return None,
        }}

        let mut plot = GTKPlot::new();
        plot.update_data(frames);
        //plot.area().set_size_request(-1, 150);
        self.content.add(&plot.widget);
        Some(plot)
    }

    pub fn finalize(self) -> gtk::Frame {
        if self.content.get_children().len() < 1 {
            self.frame.remove(&self.content);
        }
        self.frame
    }
}
