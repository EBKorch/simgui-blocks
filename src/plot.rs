use std::rc::Rc;
use std::cell::RefCell;
use gtk::prelude::*;
use sim_framework::measurement::{ Frame, Property };

use super::get_object;

const X_TICK_DEFAULT_COUNT: usize = 10;
const Y_TICK_DEFAULT_COUNT: usize = 10;
const DEFAULT_TICK_FRACTIONS: [usize; 4] = [1, 2, 4, 10];

const SIZE_SMALL: i32 = 100;
const SIZE_MEDIUM: i32 = 150;
const SIZE_LARGE: i32 = 250;

/// Represents and manages plotted frames
///
/// Underlying structure is a GtkDrawingArea where frames are plotted. However
/// the presented GUI contains added controls to change the appearance of the
/// plot.
///
/// To insert it into a GUI use the publicly accessible `widget` field which
/// contains the main widget of the plot.
pub struct GTKPlot {
    area: gtk::DrawingArea,
    pub widget: gtk::Widget,
    data: Rc<RefCell<Vec<Frame<f64>>>>,
    config: Rc<RefCell<PlotConfig>>,
}

impl GTKPlot {
    /// Creates a new `GTKPlot`
    ///
    /// This includes building the GUI and connecting events. By default there
    /// is no data on the plot.
    pub fn new() -> Self {
        // Initially the plot has no data
        let data = Rc::new(RefCell::new(vec![]));
        // Build the GUI from Glade file
        let builder = gtk::Builder::from_string(include_str!("../resources/plot.ui"));
        // The outer object is a grid: we need this as a widget to be able to
        // insert it into generic UIs
        let grid: gtk::Grid = get_object!(builder, "grid");
        let widget: gtk::Widget = grid.upcast();
        // Retrieve the grow check box
        let grow: gtk::CheckButton = get_object!(builder, "grow");
        // Get the minimum and maximum values
        let min: gtk::Adjustment = get_object!(builder, "adjustment_min");
        let min_button: gtk::SpinButton = get_object!(builder, "min_spin_button");
        let min_label: gtk::Label = get_object!(builder, "min_label");
        let max: gtk::Adjustment = get_object!(builder, "adjustment_max");
        let max_button: gtk::SpinButton = get_object!(builder, "max_spin_button");
        let max_label: gtk::Label = get_object!(builder, "max_label");
        // Get the size controlling combo box
        let height_combo_box: gtk::ComboBoxText = get_object!(builder, "height_combo_box");
        // Retrieve the drawing area
        let area: gtk::DrawingArea = get_object!(builder, "area");
        // We also set a requested hight on it
        area.set_size_request(-1, SIZE_MEDIUM);
        // Define the configuration of the GTKPlot. This wil be passed between
        // closures, so we collect the common values together.
        let config = Rc::new(RefCell::new(PlotConfig {
            min: min.get_value(),
            max: max.get_value(),
            grow: grow.get_active(),
            xtick_count_req: X_TICK_DEFAULT_COUNT,
            ytick_count_req: Y_TICK_DEFAULT_COUNT,
            tick_fractions: Vec::from(DEFAULT_TICK_FRACTIONS),
        }));

        // Create the structure
        let plot = GTKPlot { area, widget, data, config };

        // Create variables to cache the x and y axis step sizes
        let steps = Rc::new(RefCell::new(ValueCacher::new(2.0)));

        // Connect the button actions to config changes
        let config = plot.config.clone();
        let to_mark = steps.clone();
        let area = plot.area.clone();
        let max_adjustment = max.clone();
        min.connect_value_changed(move |adjustment| {
            let value = adjustment.get_value();
            config.borrow_mut().min = value;
            if value > max_adjustment.get_value() {
                max_adjustment.set_value(value);
            }
            to_mark.borrow_mut().mark();
            area.queue_draw();
        });
        let area = plot.area.clone();
        let config = plot.config.clone();
        let to_mark = steps.clone();
        let min_adjustment = min.clone();
        max.connect_value_changed(move |adjustment| {
            let value = adjustment.get_value();
            config.borrow_mut().max = value;
            if value < min_adjustment.get_value() {
                min_adjustment.set_value(value)
            }
            to_mark.borrow_mut().mark();
            area.queue_draw();
        });
        let config = plot.config.clone();
        let to_mark = steps.clone();
        let area = plot.area.clone();
        grow.connect_clicked(move |button| {
            config.borrow_mut().grow = button.get_active();
            to_mark.borrow_mut().mark();
            area.queue_draw();
        });
        let area = plot.area.clone();
        height_combo_box.connect_changed(move |combo_box| {
            let height = match combo_box.get_active_text().unwrap().as_str() {
                "S" => SIZE_SMALL,
                "M" => SIZE_MEDIUM,
                "L" => SIZE_LARGE,
                "Hidden" => {
                    // Hide everything except this combo box
                    grow.set_visible(false);
                    area.set_visible(false);
                    min_button.set_visible(false);
                    max_button.set_visible(false);
                    min_label.set_visible(false);
                    max_label.set_visible(false);
                    return
                },
                _ => unreachable!(
                    "Most possibly there is a mismatch between the resources/plot.ui \
                    file and the internal logic in the size ComboBoxText callback function."
                ),
            };
            if !grow.get_visible() {
                grow.set_visible(true);
                area.set_visible(true);
                min_button.set_visible(true);
                max_button.set_visible(true);
                min_label.set_visible(true);
                max_label.set_visible(true);
            }
            area.set_size_request(-1, height);
        });
        // Set up the draw event
        let data = Rc::clone(&plot.data);
        let config = plot.config.clone();
        plot.area.connect_draw(move |area, cr| {
            let ctx = area.get_style_context();
            let foreground_color = ctx.get_color(ctx.get_state());
            let width = area.get_allocated_width() as f64;
            let height = area.get_allocated_height() as f64;

            gtk::render_background(&ctx, cr, 0.0, 0.0, width, height);

            let length = data.borrow().len();
            if length > 1 {
                // Determine the time scale (x axis)
                let first_frame: &Frame<f64> = &data.borrow()[0];
                let last_frame: &Frame<f64> = &data.borrow()[length-1];
                let time_scale = width/(last_frame.time - first_frame.time);
                let time_offset = first_frame.time;
                // Determine the value scale (y axis)
                let config_borrow = config.borrow();
                let (mut min, mut max) = if (config_borrow.max - config_borrow.min) <= f64::EPSILON {
                    (f64::MAX, f64::MIN)
                } else {
                    (config_borrow.min, config_borrow.max)
                };
                if config_borrow.grow {
                    for frame in &data.borrow()[..] {
                        if frame.value < min { min = frame.value; }
                        else if frame.value > max { max = frame.value; }
                    }
                }
                drop(config_borrow);
                let (value_scale, value_offset) = if min < max {
                    (height/(max-min), min)
                } else {
                    (height/0.1, min+0.05)
                };
                // Define the mapping
                let map_x = |value: f64| (value-time_offset)*time_scale;
                let map_y = |value: f64| height-(value-value_offset)*value_scale;

                // Draw the background lines
                cr.set_source_rgba(
                    foreground_color.red,
                    foreground_color.green,
                    foreground_color.blue,
                    0.2
                );
                // For x axis
                if steps.borrow_mut().ping(last_frame.time) {
                    let config_borrow = config.borrow();
                    // Calculate step size for x axis
                    let x_step = GTKPlot::calc_grid_step(
                        first_frame.time,
                        last_frame.time,
                        config_borrow.xtick_count_req,
                        &config_borrow.tick_fractions,
                    );
                    // Calculate step size for the y axis
                    let y_step = GTKPlot::calc_grid_step(
                        min,
                        max,
                        config_borrow.ytick_count_req,
                        &config_borrow.tick_fractions,
                    );
                    steps.borrow_mut().update((x_step, y_step), last_frame.time);
                }
                // Draw x lines
                let (x_step, y_step) = *steps.borrow().get().unwrap();
                let start: i32 = (first_frame.time/x_step).ceil() as i32;
                let end: i32 = (last_frame.time.ceil()/x_step).ceil() as i32;
                for x in start..end {
                    cr.move_to(map_x(x as f64 * x_step), 0.0);
                    cr.line_to(map_x(x as f64 * x_step), height);
                    cr.show_text(&format!("{:.4}", x as f64 * x_step));
                }
                // Draw y lines
                let start = (min/y_step).ceil() as i32;
                let end = (max/y_step).ceil() as i32;
                for y in start..end {
                    cr.move_to(width, map_y(y as f64 * y_step));
                    cr.line_to(0.0, map_y(y as f64 * y_step));
                    cr.show_text(&format!("{:.4}", y as f64 * y_step));
                }
                cr.stroke();

                // Start plotting the data line
                cr.set_line_width(2.0);
                cr.set_source_rgba(
                    foreground_color.red,
                    foreground_color.green,
                    foreground_color.blue,
                    foreground_color.alpha
                );
                cr.move_to(0.0, map_y(first_frame.value));
                for frame in &data.borrow()[1..] {
                    cr.line_to(map_x(frame.time), map_y(frame.value));
                }
                // Actually draw the line
                cr.stroke();
            }

            gtk::Inhibit(true)
        });
        plot
    }

    /// Sets the data to the passed frames
    ///
    /// This will also mark the area to redraw
    pub fn set_data<T: Into<f64> + Clone>(&mut self, data: &Vec<Frame<T>>) {
        *self.data.borrow_mut() = data.iter().map(|f| {
            Frame { value: f.value.clone().into(), time: f.time }
        }).collect();
        self.update()
    }

    /// Updates data on the plot
    ///
    /// Currently only f64 properties are supported, so if anything else is
    /// encountered, the function panics.
    pub fn update_data(&mut self,  frames: Vec<Frame<Property>>) {
        let mut mutable = self.data.borrow_mut();
        mutable.drain(0..);
        mutable.append(&mut frames.into_iter().map(|frame| {
            match frame.value {
                Property::Float(v) => Frame { value: v, time: frame.time },
                _ => panic!("Cannot process non-f64 values"),
            }
        }).collect::<Vec<Frame<f64>>>());
        self.update();
    }

    /// Marks the area for redraw
    fn update(&self) {
        self.area.queue_draw();
    }

    // Calculates the optimal step size between min and max values if the
    // desired number of steps is given. The `ticks` argument is used to select
    // a fraction most desired for the step.
    #[inline]
    fn calc_grid_step(min: f64, max: f64, count_request: usize, ticks: &[usize]) -> f64 {
        let mut scale = 1.0;
        let interval = max-min;
        let step_estimate = interval / (count_request as f64);
        while step_estimate >= 1.0/(ticks[0] as f64)*scale {
            scale *= 10.0;
        }
        while step_estimate < 1.0/(ticks[ticks.len()-1] as f64)*scale {
            scale /= 10.0;
        }
        let mut step = (1.0/ticks[0] as f64)*scale;
        let mut diff = (step_estimate-step).abs();
        for tick in ticks[1..].iter() {
            let new_step = 1.0/(*tick as f64)*scale;
            let new_diff = (step_estimate-new_step).abs();
            if new_diff < diff {
                diff = new_diff;
                step = new_step;
            } else { break; }
        }
        step
    }
}

// Contains common plot configuration options
//
// This is passed between buttons and drawing functions to make it easier to
// share common data.
struct PlotConfig {
    max: f64,
    min: f64,
    grow: bool,
    xtick_count_req: usize,
    ytick_count_req: usize,
    tick_fractions: Vec<usize>,
}

// Simple cacher which notifies when its time for update
//
// Elapsed time s calculated from real time, eg. through the argument `time`
struct ValueCacher<T> {
    interval: f64,
    last: f64,
    value: Option<T>,
    marked: bool,
}

impl<T> ValueCacher<T> {
    // Creates a new cacher
    //
    // Here you can set update interval in seconds
    pub fn new(interval: f64) -> Self {
        ValueCacher { interval, value: None, last: -f64::MAX, marked: false }
    }

    // Checks if the cacher needs update
    //
    // Pass the current time for the function
    pub fn ping(&mut self, time: f64) -> bool {
        if !self.marked && (time - self.last) > self.interval && self.value.is_some() {
            false
        } else {
            true
        }
    }

    // Marks the cacher for update
    //
    // Tis will make the cacher ignore the time constrain
    pub fn mark(&mut self) {
        self.marked = true;
    }

    // Returns with the cached value
    pub fn get(&self) -> Option<&T> {
        match &self.value {
            Some(value) => Some(value),
            None => None,
        }
    }

    // Updates the cached value
    //
    // If the cacher is marked, it will be erased here. The last update time
    // is also updated.
    pub fn update(&mut self, value: T, time: f64) {
        self.value = Some(value);
        self.last = time;
        self.marked = false;
    }
}
